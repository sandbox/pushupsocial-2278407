<?php

/**
 * @file
 * Administrative page callbacks for the pushupsocial module.
 */

/**
 * Implements hook_admin_settings().
 */
function pushupsocial_admin_settings_form($form, $form_state) {

  $community_id = variable_get('pushupsocialCommunityId', '');
  $is_selected = variable_get('pushupsocialEnabled') == 1 ? ' active' : '';

  $query_params = drupal_get_query_parameters();
  $show_form_flag = FALSE;

  if (count($query_params) > 0) {
    if (isset($query_params[PUSHUPSOCIAL_PAGESLUG_CONFIG])) {
      $show_form_flag = TRUE;
    }
  }

  if (!$community_id && !$show_form_flag) {
    // Splash Screen.
    $form['splash'] = array(
      '#markup' => pushupsocial_splash(),
      '#id' => 'pushup-social-splash',
    );
  }
  else {
    // Community ID.
    $form['pushupsocialCommunityId'] = array(
      "#type" => "textfield",
      '#title' => t('Community ID'),
      '#default_value' => $community_id,
      '#size' => 40,
      '#description' => t("<p class=\"pushup-social-helptext\">If you don't
        have a community ID, <a href=\"@pushup_join_link\" target=\"_blank\">
        Sign Up Here</a> When you sign up, you will be sent your community ID
        in an email. Please allow some time for your community ID to be
        generated.</p>", array('@pushup_join_link' => PUSHUPSOCIAL_LINK_JOIN)),
      '#required' => TRUE,
      '#element_validate' => array('pushupsocial_community_id_element_validate'),
    );

    // Enable Pushup Bar.
    $form['pushupsocialEnabled'] = array(
      '#type' => 'checkbox',
      '#default_value' => variable_get('pushupsocialEnabled', 0),
      '#prefix' => '<label>Community State</label><div class="onoffswitch">',
      '#suffix' => '<label class="onoffswitch-label' . $is_selected .
      '" for="edit-pushupsocialenabled"><div class="onoffswitch-inner"></div>' .
      '<div class="onoffswitch-switch"></div></label></div>',
    );

    // Attach switch javascript functionality.
    $form['#attached']['js'] = array(drupal_get_path('module', 'pushupsocial') .
      '/js/pushupsocial.onoffswitch.js');
    $form = system_settings_form($form);
  }

  return $form;
}

/**
 * Implements hook_field_validate().
 */
function pushupsocial_community_id_element_validate(&$element, &$form_state) {
  $value = $element['#value'];

  // Check for a valid community id.
  if (strlen($value) != 24) {
    form_error($element, t("The %element-title you've entered is invalid.
      Please enter a valid %element-title.",
      array('%element-title' => $element['#title'])));
  }

  return $element;
}

/**
 * Pushup social splash.
 */
function pushupsocial_splash() {
  $pushupsocial_splash = <<<PUSHUPSOCIAL_SPLASH
<div class="pushup-social-panel" style="background-image: url(@bg_src)">
    <img class="pushup-social-logo" src="@logo_src"/>

    <p class="pushup-social-description">
      Add a social network or community to your existing website in minutes!
      Build and grow "your own" community on "your own" website without having
      to rebuild or redesign your website.
    </p>
    <p>
      <a class="pushup-social-conversion-btn" role="button" target="_blank" href="@join">Sign up</a>
      <a class="pushup-social-conversion-btn blue" role="button" href="?@config">I have my code</a>
    </p>

    <p>
      <a role="button" class="pushup-social-learn-more" target="_blank" href="@find">Learn More</a>
    </p>

</div>
PUSHUPSOCIAL_SPLASH;

  $pushupsocial_links = array(
    '@join' => PUSHUPSOCIAL_LINK_JOIN,
    '@config' => PUSHUPSOCIAL_PAGESLUG_CONFIG,
    '@find' => PUSHUPSOCIAL_FIND,
    '@logo_src' => PUSHUPSOCIAL_LOGO_PATH,
    '@bg_src' => PUSHUPSOCIAL_BG_PATH,
  );

  return format_string($pushupsocial_splash, $pushupsocial_links);
}
