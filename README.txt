CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Installation
 * Configuration
 * FAQ
 * Maintainers


INTRODUCTION
------------

Pushup Social is the easiest way to add a social network to your
Drupal site. Simply register a community and enter the ID on the
settings page.

Pushup Social is a technology platform that allows you add a social
network to any existing website in a matter of seconds. This plugin
automatically inserts the Pushup Social snippet into the header of
your website


INSTALLATION
------------
 1. Upload module zip through your Drupal installations Module menu
    or find "Pushup Social" in the Drupal web portal
 2. Activate the module through the ‘Modules’ menu in Drupal


FAQ
---
Q: How do I sign up for a community? =
A: [Sign up for Pushup](http://pushup.com/) and all
the information will be emailed to you.


MAINTAINERS
-----------
Current maintainers:
 * PushupSocial - https://drupal.org/user/2902273
