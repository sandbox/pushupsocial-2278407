/**
 * @file
 * Admin community state toggle switch.
 */

(function ($) {

  Drupal.behaviors.pushupSocialOnOffSwitch = {
    attach: function (context, settings) {
      var $pushupsocialActivateCheckbox = $('#edit-pushupsocialenabled', context),
          $pushupsocialActivateLabel = $(".onoffswitch-label", context);

      _toggleActiveState();

      $pushupsocialActivateCheckbox.bind('change', function () {
        _toggleActiveState();
      });

      function _toggleActiveState () {
        if ($pushupsocialActivateCheckbox.attr('checked')) {
          $pushupsocialActivateLabel.addClass('active');
        } else {
          $pushupsocialActivateLabel.removeClass('active');
        }
      }
    }
  }

})(jQuery);
